models/mapobjects/machinatrium/stairwell-downstairs
{
	surfaceparm nolightmap
	{
		map models/mapobjects/machinatrium/stairwell-downstairs.tga
	}
}

models/mapobjects/machinatrium/stairwell-stairs
{
	surfaceparm nolightmap
	{
		map models/mapobjects/machinatrium/stairwell-stairs.tga
	}
}

models/mapobjects/machinatrium/stairwell-upstairs
{
	surfaceparm nolightmap
	{
		map models/mapobjects/machinatrium/stairwell-upstairs.tga
	}
}

models/mapobjects/machinatrium/outreach-u
{
	surfaceparm nolightmap
	{
		map models/mapobjects/machinatrium/outreach-u.tga
	}
}

models/mapobjects/machinatrium/underpass-tunnel
{
	surfaceparm nolightmap
	{
		map models/mapobjects/machinatrium/underpass-tunnel.tga
	}
}

textures/skies/machinatrium
{
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm sky

        skyparms models/mapobjects/machinatrium/sky - -
       {
	}
}